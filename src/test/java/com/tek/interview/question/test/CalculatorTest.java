package com.tek.interview.question.test;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.tek.interview.question.Calculator;
import com.tek.interview.question.Item;
import com.tek.interview.question.Order;
import com.tek.interview.question.OrderLine;

public class CalculatorTest {

	@Before
	public void setUp() {
		
	}
	
	@Test
	public void testCalculate() throws Exception {
		Calculator cal = new Calculator();
		Map<String, Order> firstOrderMap = new LinkedHashMap<String, Order>();
		Map<String, Order> secondOrderMap = new LinkedHashMap<String, Order>();
		
		double firstOrderTotal =  28.33;
		Order firstOrder = new Order();
		firstOrder.add(new OrderLine(new Item("book", (float) 12.49), 1));
		firstOrder.add(new OrderLine(new Item("music CD", (float) 14.99), 1));
		firstOrder.add(new OrderLine(new Item("chocolate bar", (float) 0.85), 1));
		firstOrderMap.put("Order 1", firstOrder);
		cal.calculate(firstOrderMap);
		
		double secondOrderTotal =  57.5;
		Order secondOrder = new Order();
		secondOrder.add(new OrderLine(new Item("imported box of chocolate", 10), 1));
		secondOrder.add(new OrderLine(new Item("imported bottle of perfume", (float) 47.50), 1));

		secondOrderMap.put("Order 2", secondOrder);
		new Calculator().calculate(secondOrderMap);
		
		
		double actual = firstOrderTotal + secondOrderTotal;
		Assert.assertNotNull(cal.getSumOfOrders());
		Assert.assertEquals(cal.getSumOfOrders(), actual,2);
	}
}