package com.tek.interview.question.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.tek.interview.question.Item;
import com.tek.interview.question.Order;
import com.tek.interview.question.OrderLine;

public class OrderTest {

	private List<OrderLine> orderLines = new ArrayList<OrderLine>();
	
	OrderLine actualOrderLine;
	
	@Before
	public void setUp() throws Exception{
		Item item = new Item("book", (float) 12.49);
		actualOrderLine = new OrderLine(item,2);
	}
	
	@Test(expected=Exception.class)
	public void shouldThrowExceptionWhenOrderLineIsNull() throws Exception{
		Order order = new Order();
		order.add(actualOrderLine);
		
		order.add(null);
	}
	
	@Test
	public void testGetItem() throws Exception{
		Order order = new Order();
		order.add(actualOrderLine);
		
		OrderLine expected = order.get(0);
		
		Assert.assertEquals(expected, actualOrderLine);
	}
	
	@Test
	public void testGetSize() throws Exception{
		Order order = new Order();
		order.add(actualOrderLine);
		orderLines.add(order.get(0));
		int expected = order.size();
		
		Assert.assertEquals(expected, orderLines.size());
	}
	
	@Test
	public void testClear() throws Exception{
		Order order = new Order();
		order.add(actualOrderLine);
		orderLines.add(order.get(0));
		orderLines.clear();
		
		Assert.assertEquals(0, orderLines.size());
	}
}
