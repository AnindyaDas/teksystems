package com.tek.interview.question.test;

import org.junit.Test;

import com.tek.interview.question.Item;
import com.tek.interview.question.OrderLine;

import junit.framework.Assert;

public class OrderLineTest {

	@Test(expected=Exception.class)
	public void shouldThrowExceptionWhenItemIsNull() throws Exception {
		OrderLine orderLine = new OrderLine(null,1);
	}
	
	@Test
	public void test() throws Exception {
		Item item = new Item("book", (float) 12.49);
		OrderLine orderLine = new OrderLine(item,1);
		Assert.assertEquals(item,orderLine.getItem());
		Assert.assertEquals(orderLine.getQuantity(),1);
	}
}
