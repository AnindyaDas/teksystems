package com.tek.interview.question.test;

import org.junit.Before;
import org.junit.Test;

import com.tek.interview.question.Item;

import junit.framework.Assert;

public class ItemTest {

	@Before
	public void setUp() {
		
	}
	
	@Test
	public void testGetDescription(){
		String actual ="book";
		Item item = new Item(actual, (float) 12.49); 
		Assert.assertEquals(item.getDescription(), actual);
	}
	

	@Test
	public void testGetPrice(){
		float actual = 12.49f;
		Item item = new Item("book", actual ); 
		Assert.assertEquals(item.getPrice(), actual);
	}
	
}

