package com.tek.interview.question;

import java.util.Map;

public class Calculator {
	
	private static double grandtotal = 0;
	
	private static double rounding(double value) {
		
		return (( Math.ceil(value * 100)) / 100);
	}
	
	/**
	 * receives a collection of orders. For each order, iterates on the order lines and calculate the total price which
	 * is the item's price * quantity * taxes.
	 * 
	 * For each order, print the total Sales Tax paid and Total price without taxes for this order
	 */
	public void calculate(Map<String, Order> o) {

		 

		// Iterate through the orders
		for (Map.Entry<String, Order> entry : o.entrySet()) {
			System.out.println("*******" + entry.getKey() + "*******");

			Order r = entry.getValue();

			double totalTax = 0;
			double totalPriceWithTax = 0;//total price with tax
			double totalPriceWithoutTax =0;

			// Iterate through the items in the order
			for (int i = 0; i < r.size(); i++) {

				// Calculate the taxes
				double tax = 0;
			

				if (r.get(i).getItem().getDescription().contains("imported")) {
					tax = (r.get(i).getItem().getPrice() * 0.15); // Extra 5% tax on
					// imported items
				} else {
					tax = (r.get(i).getItem().getPrice() * 0.1);
				}

				// Calculate the price with tax
 				double priceWithTax = r.get(i).getItem().getPrice() + tax;
				
				

				// Print out the item's total price
				System.out.println(r.get(i).getItem().getDescription() + ": " + rounding(priceWithTax));

				// Keep a running total
				totalTax += tax;
				
				totalPriceWithTax += priceWithTax;
			}
			
			// Print out the total taxes
			System.out.println("Sales Tax: " +rounding(totalTax));

			totalPriceWithoutTax = totalPriceWithTax - totalTax;
                  
			// Print out the total amount
			System.out.println("Total: " + rounding(totalPriceWithoutTax));
			grandtotal += totalPriceWithoutTax;
		}

		
	}
	
	public void printSumOfOrders(){
		System.out.println("Sum of orders: " + rounding(grandtotal));
	}
	
	public double getSumOfOrders() {
		return grandtotal;
	}
	    

}
