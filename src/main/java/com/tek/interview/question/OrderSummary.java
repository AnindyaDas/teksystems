package com.tek.interview.question;

import java.util.LinkedHashMap;
import java.util.Map;

public class OrderSummary {
	
	public static void main(String[] args) throws Exception {

		Map<String, Order> firstOrderMap = new LinkedHashMap<String, Order>();
		Map<String, Order> secondOrderMap = new LinkedHashMap<String, Order>();
		Map<String, Order> thirdOrderMap = new LinkedHashMap<String, Order>();

		Order firstOrder = new Order();
        
		firstOrder.add(new OrderLine(new Item("book", (float) 12.49), 1));
		firstOrder.add(new OrderLine(new Item("music CD", (float) 14.99), 1));
		firstOrder.add(new OrderLine(new Item("chocolate bar", (float) 0.85), 1));

		firstOrderMap.put("Order 1", firstOrder);
		new Calculator().calculate(firstOrderMap);
		
		// Reuse cart for an other order
		firstOrder.clear();

		Order secondOrder = new Order();
		secondOrder.add(new OrderLine(new Item("imported box of chocolate", 10), 1));
		secondOrder.add(new OrderLine(new Item("imported bottle of perfume", (float) 47.50), 1));

		secondOrderMap.put("Order 2", secondOrder);
		new Calculator().calculate(secondOrderMap);

		// Reuse cart for an other order
		secondOrder.clear();

		Order thirdOrder = new Order();
		thirdOrder.add(new OrderLine(new Item("imported bottle of perfume", (float) 27.99), 1));
		thirdOrder.add(new OrderLine(new Item("bottle of perfume", (float) 18.99), 1));
		thirdOrder.add(new OrderLine(new Item("packet of headache pills", (float) 9.75), 1));
		thirdOrder.add(new OrderLine(new Item("box of imported chocolates", (float) 11.25), 1));

		thirdOrderMap.put("Order 3", thirdOrder);
        new Calculator().calculate(thirdOrderMap);
        new Calculator().printSumOfOrders();
        thirdOrder.clear();
	}

}
